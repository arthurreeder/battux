# Bat Tux Transition

Using Blender I tried to rebuild the 60's Batman transition. I used a stock photo from [Casey Horner](https://unsplash.com/photos/YtSoipXxt2A). The image is under the [Unsplash Licence](https://unsplash.com/license). Everything else here is just Public Domain or [CC0](https://creativecommons.org/choose/zero/), whichever works best for you.
